!function($) {
    "use strict";
    var Chart = function(containerstr, params) {

        try {
            this.container = document.querySelector(containerstr);
            this.container.className = "chart";
            this.container.str = containerstr;

            // default params                                                                                
            this.margin = { top: 20, right: 20, bottom: 70, left: 60, };
            this.width = 480;
            this.height = 320;
            this.fontsize0 = "16px";
            this.fontsize1 = "10px";
            this.nplots = 1;
            this.legend = ['', ];
            this.reverty = false;
            if (typeof params === 'undefined') {
                throw('Chart(container_id, param) requires param.');
            }
            for (var key in params) {
                this[key] = params[key];
            }
            var required = ['loader', ];
            for (var i = 0; i < required.length; i++) {
                if (!this[required[i]]) {
                    throw('Chart(container_id, param) requires param.' + required[i] + '.');
                }
            }

            // init vars                                                                                     
            this.width0 = this.width - this.margin.left - this.margin.right;
            this.height0 = this.height - this.margin.top - this.margin.bottom;

            // initial plot                                                                                  
            this.new_plot();
        }
        catch (e) {
            console.log(e);
        }
    };

    Chart.prototype.set_loader = function(loader) {
        this.loader = loader;
    }

    Chart.prototype.load_data = function() {
        var me = this;
        var promise = this.loader(me).then(function(res){
            me.data = res;
            var xmax = null;
            var ymax = null;
            for (var j in me.data) { // each divelog
                var p = me.data[j].profile;
                for (var i in p) { // each tic
                    var y = p[i][me.cols[0]];
                    if (y == null) continue;
                    if (ymax == null) ymax = y;
                    if (y > ymax) ymax = y;
                }
                var x = p[p.length - 1][0];
                if (xmax == null || xmax < x) {
                    xmax = x;
                }
            }
            me.xdomain = [0.0, xmax];
            if (!me.ydomain) me.ydomain = [0.0, ymax];
        });
        return promise;
    }

    Chart.prototype.new_plot = function(){
        var me = this;
        this.load_data().then(function() {
            me.plot_d3();
        });
    };

    Chart.prototype.plot_d3 = function() {
        var me = this;
        var divid = me.container.str;
        d3.select(divid).select("svg").remove();

        var svg = d3.select(divid).append("svg")
            .attr("width", me.width)
            .attr("height",me.height)
            .append("g")
            .attr("class", "base")
            .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

        me.scale_xy();

        me.xaxis = d3.axisBottom()
            .scale(me.xscale)
            .ticks(10)

        me.yaxis = d3.axisLeft()
            .scale(me.yscale)
            .ticks(5)

        me.line = [];
        var plots = [], cols = [];
        for (var j = 0; j < me.nplots; j++) {
            plots[j] = j;
            me.line.push([]);
        }
        for (var i = 0; i < me.cols.length; i++) {
            cols[i] = i;
        }
        plots.forEach(function(j) { // note!: for(var col...) does not work.
            cols.forEach(function(i) {
                me.line[j][i] = d3.line()
                    .x(function(d, k){
                        var x = me.xscale(d[0]);
                        return x;
                    })
                    .y(function(d){
                        return me.yscale(d[me.cols[i]]);
                    })
            })
        });

        // data plot                                                                                         
        var clippath = svg.append("clipPath")
            .attr("id", "clip0")
            .append("rect")
            .attr("id", "rect0")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", me.width0)
            .attr("height", me.height0)

        d3.select(divid).on("mousemove", function() {
            var area = document.getElementById("depth-chart");
            var pos = d3.mouse(area);
            var ml = me.margin.left;
            var time = me.xscale.invert(pos[0] - ml);
            var i = time2index(time);
            time = time.toFixed(1);
            // if (time < 0.0 || me.xdomain[1] < time) return;
            if (i <= 0 || me.data[0].profile.length <= i) return;
            var p = me.data[0].profile;
            var depth = p[i][1].toFixed(1);
            var vel = p[i][2].toFixed(2);
            guideLine
                .transition()
                .duration(10)
                .attr("x1", pos[0] - ml * 2)
                .attr("x2", pos[0] - ml * 2)
            tooltip
                .attr("x", pos[0] - ml)
                .text(time ? time + 's  ' + depth + 'm ' + vel + 'm/s': "")

            function time2index(t) {
                var p = me.data[0].profile;
                var i = 0;
                while (true) {
                    if (i >= p.length || p[i][0] >= time) break;
                    i++;
                }
                return i;
            }
        });

        var overLayer = svg.append("g").attr("class", "overLayer")
            .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

        var guideLine = overLayer.append("line")
            .attr("class", "guideLine")
            .attr("x1", 0)
            .attr("y1", -me.margin.top + 30)
            .attr("x2", 0)
            .attr("y2", -me.margin.top + me.height0)
            .attr("stroke", "#aaaaaa")

        var tooltip = svg.append("g").append("text")
            .style("font-size", me.fontsize0)
            .attr("class", "label")
            .attr("x", 0)
            .attr("y",  me.margin.top)
            .attr("text-anchor", "middle")
            .text("");

        var opacity = [1.0, 0.4, 0.16];
        var stroke_color = ["#4444ff", "#22cccc", "#22aaaa", ];
        for (var j = 0; j < me.nplots; j++) {
            for (var i = 0; i < me.cols.length; i++) {
                var st = stroke_color[Math.trunc((i+j) / opacity.length)];
                var op = opacity[(i+j) % opacity.length];
                svg.append("g")
                    .attr("clip-path", "url(#clip0)")
                    .append("path")
                    .attr("class", "line")
                    .attr("stroke", st)
                    .attr("opacity", op)
                    .attr("id", "line" + j + "_" + i);
            }
        }

        for (var j = 0; j < me.nplots; j++) {
            for (var i = 0; i < me.cols.length; i++) {
                var id = 'line' + j + '_' +i;
                var d = svg.selectAll("path#" + id).datum(me.data[j].profile);
                d.attr("d", me.line[j][i]);
            }
        }

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0,  " + me.height0 + ")")
            .call(me.xaxis)
            .selectAll("text")
            .style("font-size", me.fontsize1)
            .style("text-anchor", "center")
            .attr("transform", "rotate(-20)");

        svg.append("g")
            .attr("class", "y axis")
            .call(me.yaxis)
            .selectAll("text")
            .style("font-size", me.fontsize1);

        svg.append("g")
            .append("text")
            .style("font-size", me.fontsize0)
            .attr("class", "ylabel")
            .attr("x", -200)
            .attr("y", -40)
            .attr("text-anchor", "left")
            .attr("transform", "rotate(-90)")
            .text(me.ylabel);

        svg.append("g")
            .append("text")
            .style("font-size", me.fontsize0)
            .attr("class", "xlabel")
            .attr("x", me.width0 / 2.0)
            .attr("y",  me.height0 + 40)
            .attr("text-anchor", "middle")
            .text(me.xlabel);

        var xr = 0.7;
        var yoff = 20 * (me.nplots + me.cols.length);
        for (var j = 0; j < me.nplots; j++) {
            for (var i = 0; i < me.cols.length; i++) {
                if (!me.legend[i]) continue;
                var st = stroke_color[Math.trunc((i+j) / opacity.length)];
                var op = opacity[(i+j) % opacity.length];
                var id = 'legend' + i;
                svg.append("g")
                    .append("text")
                    .text(me.legend[i+j])
                    .attr("stroke", st)
                    .attr("opacity", op)
                    .attr("fill", st)
                    .attr("x", me.width0 * xr)
                    .attr("y",  me.margin.top + me.height0 + 20 * (i + j) - yoff)
                    .attr("class", "legend")
                    .attr("id", id)
                    .style("text-anchor", "left")
                    .style("font-size", me.fontsize0)

                id = 'legend-line' + i;
                svg.append("g")
                    .append("line")
                    .attr("class", "legend-line")
                    .attr("id", id)
                    .attr("stroke", st)
                    .attr("opacity", op)
                    .attr("x1", me.width0 * xr - 60)
                    .attr("x2", me.width0 * xr - 20)
                    .attr("y1", me.margin.top + me.height0 + 20 * (i + j) - yoff - 4)
                    .attr("y2", me.margin.top + me.height0 + 20 * (i + j) - yoff - 4)
            }
        }
    };

    Chart.prototype.scale_xy = function(){
        var me = this;
        this.xscale = d3.scaleLinear()
            .domain(me.xdomain)
            .range([0, me.width0]);
        var yd;
        if (me.reverty) {
            yd = [me.ydomain[1], me.ydomain[0]];
        }
        else {
            yd = [me.ydomain[0], me.ydomain[1]];
        }
        me.yscale = d3.scaleLinear()
            .domain(yd)
            .range([me.height0, me.margin.top]);
    };

    // export the constructor
    window.Chart = Chart;

}(jQuery);
