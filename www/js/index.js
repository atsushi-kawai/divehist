!function($) {
    "use strict";

    var NotLoadedYet = true;
    var SampleXmls = ["./sample.xml", ];
    var DepthChartId = '#depth-chart';
    var VelChartId = '#vel-chart';
    var Divelogs = [];
    // {date:'2019-05-02T09:44:18.05', profile: [ [t0, d0, v0, sv0], [t1, d1, v1, sv1],...]}

    function init_model() {
        var $dfd = $.Deferred();

        // load sample data
        $.get(SampleXmls[0]).then(function(xml){
            Divelogs.push(xml2jsobj(xml));
            init_view();
            $dfd.resolve();
        }, function(e) {
            console.log('error:', e);
        });
        return $dfd.promise();
    }

    function init_view() {
        init_radio_view();
        init_ff_view();
        init_table_view();
        init_plot_view();
    }

    function init_ff_view() {
        var h = '';
        if (NotLoadedYet) {
            h += '<b>Freefall starts at: &emsp; &emsp; &emsp; &emsp;</b>';
        }
        if (NotLoadedYet || Divelogs.length > 1) {
            var j = Divelogs.length - 1;
            h += '<span>';
            h += '<input type="number" size="2" class="ff-text" id="ff-text' + j + '" value="20" style="width:40px;"></input> <b>m</b>';
            h += '</span> &emsp; &emsp;';
        }
        $('#ff-field').append(h);
        $('.ff-text').change(function() {
            init_table_view();
        });
    }

    function init_radio_view() {
        var $r = $('#smoothed');
        if (NotLoadedYet) {
            $r.find('.btn[value=smoothed]').addClass('active');
        }
        if (Divelogs.length == 1) {
            $r.hide();
        }
        else {
            $r.show();
        }
        $r.find('.btn').click(function() {
            $r.find('.btn').removeClass('active');
            $(this).addClass('active');
            init_plot_view();
        });
    }

    function init_table_view() {
        var date = [], divet = [], maxd = [], maxdat = [], ffat0 = [];
        var av0 = [], av1 = [], av2 = [];
        for (var j in Divelogs) {
            var p = Divelogs[j].profile;
            var ffat = $('#ff-text' + j).val();
            console.log("ffat:", ffat);
            var md = 0.0, mdat = 0;
            var d0 = 0, t0 = 0;
            for (var i in p) {
                var t = p[i][0];
                var d = p[i][1];
                if (d > md) {
                    md = d;
                    mdat = t;
                }
                if (d0 < ffat && ffat < d) {
                    d0 = d;
                    t0 = t;
                }
            }
            var dt = p[p.length - 1][0]; // the 1st elem of the last elem of profile.
            date[j] = Divelogs[j].date;
            divet[j] = dt;
            maxd[j] = md;
            maxdat[j] = mdat;
            ffat0[j] = d0;
            av0[j] = (d0 / t0).toFixed(2);
            av1[j] = ((md - d0) / (mdat - t0)).toFixed(2);
            av2[j] = (md / (dt - mdat)).toFixed(2);
        }
        var h = '';
        var rows = [
            ['Start time:', date, ''],
            ['Dive time:', divet, 's'],
            ['Max depth:', maxd, 'm'],
            ['Max depth at :', maxdat, 's'],
            ['Average velocity', '', ''],
            ['&emsp;initial kicking phase:', av0, 'm/s'],
            ['&emsp;freefall to bottom:', av1, 'm/s'],
            ['&emsp;ascent (bottom to surface):', av2, 'm/s'],
        ];
        for (var i in rows) {
            var row = rows[i];
            var bg = '';
            bg = i % 2 == 0 ? '' : 'background-color:#f2f2f2;';
            h += '<tr style="' + bg + '">';
            h += '<th>' + row[0] +'</th>';
            for (var j in Divelogs) {
                h += '<td align="right">' + (row[1] == '' ? '' : row[1][j]) +'</td>';
                h += '<th>' + row[2] +'</th>';
            }
            h += '</tr>';
        }
        $('#info-table').find('tr').remove();
        $('#info-table').append(h);
    }

    function init_plot_view() {
        var legend = [];
        for (var j in Divelogs) {
            legend.push(Divelogs[j].date);
        }

        var dparam = {
            width: 768,
            height: 384,
            margin: { top: 20, right: 50, bottom: 50, left: 70, },
            loader: loader,
            xlabel: 'dive time (s)',
            ylabel: 'depth (m)',
            unit: 'm',
            reverty: true,
            cols: [1,],
            nplots: Divelogs.length,
            legend: legend,
        };
        new Chart(DepthChartId, dparam);

        // set ydomain so that the smoothed curve fits inside the chart.
        var ymin = 100, ymax = 0;
        for (var j in Divelogs) {
            var p = Divelogs[j].profile;
            for (var i in p) {
                var s = p[i][3];
                if (s > ymax) {
                    ymax = s;
                }
                if (s < ymin) {
                    ymin = s;
                }
            }
        }
        var vparam = {
            width: 768,
            height: 384,
            margin: { top: 20, right: 50, bottom: 50, left: 70, },
            loader: loader,
            xlabel: 'dive time (s)',
            ylabel: '|velocity| (m/s)',
            ydomain: [ymin - 0.08, ymax + 0.08],
            unit: 'm/s',
            cols: [3, 2,],
            nplots: Divelogs.length,
            legend: ['smoothed', 'raw', ],
        };
        if (Divelogs.length > 1) {
            var c =$('#smoothed').find('.active').attr('value') == 'smoothed' ? 3 : 2;
            vparam.cols = [c, ];
            vparam.legend = legend;
        }
        new Chart(VelChartId, vparam);
    }

    function loader() {
        var $dfd = new $.Deferred();
        $dfd.resolve(Divelogs);
        return $dfd.promise();
    }

    $('#load-xml-hidden').change(function(event){
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function(f){
            var text = f.target.result;
            var dp = new DOMParser();
            var xml = dp.parseFromString(text, "application/xhtml+xml");
            if (xml.documentElement.nodeName == "parseerror") alert("failed to parse XML.");
            if (NotLoadedYet) {
                NotLoadedYet = false;                
                $('.sampletext').hide();
                Divelogs = [];
            }
            Divelogs.push(xml2jsobj(xml));
            init_view();
        };
    });

    function xml2jsobj(xml) {
        var format = 'DM5';
        var divelog = {
            date: null,
            profile: [],
        };
        var sample = xml.getElementsByTagName("Dive")[0];
        if (sample) { // assume DM5 format by default
            sample = sample.getElementsByTagName("DiveSamples")[0]
                .getElementsByTagName("Dive.Sample");
            divelog.date = xml.getElementsByTagName("StartTime")[0].childNodes[0].nodeValue;
        }
        else { // next assume MacDive format.
            sample = xml.getElementsByTagName("dives")[0];
            if (sample) {
                format = 'MacDive';
                sample = sample.getElementsByTagName("dive")[0]
                    .getElementsByTagName("sample");
                divelog.date = xml.getElementsByTagName("date")[0].childNodes[0].nodeValue;
            }
            else {
                sample = xml.getElementsByTagName("uddf")[0];
                if (sample) {
                    format = 'MacDiveiOS';
                    sample = sample.getElementsByTagName("profiledata")[0]
                        .getElementsByTagName("repetitiongroup")[0]
                        .getElementsByTagName("dive")[0];
                    divelog.date = sample.getElementsByTagName("datetime")[0].childNodes[0].nodeValue;
                    sample = sample.getElementsByTagName("samples")[0]
                        .getElementsByTagName("waypoint");
                }
                else {
                    alert("Cannot parse the XML file. Only files generated by Suunto DM5 or MacDive can be read.");
                    return null;
                }
            }
        }

        // time and depth
        var time = [], depth = [], vel = [];
        depth[0] = time[0] = 0;
        for (var i = 0; i < sample.length; i++) {
            switch(format) {
            case 'DM5':
                time[i] = +sample[i].getElementsByTagName("Time")[0].childNodes[0].nodeValue;
                depth[i] = +sample[i].getElementsByTagName("Depth")[0].childNodes[0].nodeValue;
                break;
            case 'MacDive':
                time[i] = +sample[i].getElementsByTagName("time")[0].childNodes[0].nodeValue;
                depth[i] = +sample[i].getElementsByTagName("depth")[0].childNodes[0].nodeValue;
                break;
            case 'MacDiveiOS':
                var t = sample[i].getElementsByTagName("time");
                if (t.length == 0) {
                    time[i] = +sample[i].getElementsByTagName("divetime")[0].childNodes[0].nodeValue;
                }
                else {
                    time[i] = t[0].childNodes[0].nodeValue;
                }
                depth[i] = +sample[i].getElementsByTagName("depth")[0].childNodes[0].nodeValue;
                break;
            }
        }

        // velocity
        vel[0] = 0.0;
        for (var i = 1, v = 0.0; i < sample.length; i++) {
            vel[i] = v;
            v = Math.abs(depth[i] - depth[i-1]) / (time[i] - time[i-1]);
            if (v === Infinity || v === -Infinity) v = vel[i];
        }

        // smoothed velocity
        var avel = [];
        for (var i = 0; i < sample.length; i++) {
            var w = 10;
            var n = 0;
            avel[i] = 0.0;
            for (var j = -w; j <= +w; j++) {
                if (i+j < 0 || sample.length <= i+j) continue;
                avel[i] += vel[i+j];
                n += 1;
            }
            avel[i] /= n;
        }

        for (var i = 0; i < sample.length; i++) {
            divelog.profile[i] = [time[i], depth[i], vel[i], avel[i], ];
        }
        return divelog;
    }

    $('#load-xml').on('click', function() {
        $('#load-xml-hidden').click();
    });


    $(function init() {
        var dev;
        window.My = new MyUtil();
        init_model();
    });

}(jQuery);
