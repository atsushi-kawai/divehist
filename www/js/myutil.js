!function($) {
    "use strict";

    // private, class vars
    var Deploy = true;
    var NSandClock = 0;

    window.onerror = function(msg, url, line) {
        alert(msg + "\n" + url + ":" + line);
    };

    var MyUtil = function() {
        try {
            if ("undefined"== typeof document) return false;
            this.mega = 1024 * 1024;
            this.giga = 1024 * 1024 * 1024;
        }
        catch (e) {
            console.log(e);
        }
    };


    MyUtil.prototype.show_sandclock = function() {
        NSandClock += 1;
        $('#indicator').show();
    };

    MyUtil.prototype.hide_sandclock = function() {
        NSandClock -= 1;
        if (NSandClock <= 0) {
            NSandClock = 0;
            $('#indicator').hide();
        }
    };


    MyUtil.prototype.date_to_string = function(date, format) {
        var monthname = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
        ];
        var dayofweekname = [
            'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
        ];

        if (!format) format = '%Y/%m/%d %H:%M:%S';

        format = format.replace(/%Y/g, date.getFullYear());
        format = format.replace(/%m/g, ('0' + (date.getMonth() + 1)).slice(-2));
        format = format.replace(/%d/g, ('0' + date.getDate()).slice(-2));
        format = format.replace(/%H/g, ('0' + date.getHours()).slice(-2));
        format = format.replace(/%M/g, ('0' + date.getMinutes()).slice(-2));
        format = format.replace(/%S/g, ('0' + date.getSeconds()).slice(-2));
        format = format.replace(/%b/g, monthname[date.getMonth()]);
        format = format.replace(/%a/g, dayofweekname[date.getDay()]);
        return format;
    };

    // truncate src to the nearest dsrc aligned value smaller than src.
    MyUtil.prototype.align_floor = function(src, dsrc) {
        var dst = (Math.floor(src / dsrc)) * dsrc;
        //    console.log("src:" + src + " dst:" + dst + " dsrc:" + dsrc);
        return dst;
    }

    // round up src to the nearest dsrc aligned value larger than src.
    MyUtil.prototype.align_ceil = function(src, dsrc) {
        var dst = (Math.ceil(src / dsrc)) * dsrc;
        //    console.log("src:" + src + " dst:" + dst);
        return dst;
    }

    // export the constructor
    window.MyUtil = MyUtil;
}(jQuery)
