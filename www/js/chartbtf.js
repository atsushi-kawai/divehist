!function($) {
    "use strict";
    var Chart = function(containerstr, params) {
        try {
            this.container = document.querySelector(containerstr);
            this.container.className = "chart";
            this.container.str = containerstr;

            // default params
            this.margin = { top: 20, right: 20, bottom: 70, left: 60, };
            this.width = 480;
            this.height = 320;
            this.title = '';
            this.fontsize0 = "12px";
            this.fontsize1 = "10px";
            this.nplots = 1;
            this.legend = [ ];
            if (typeof params === 'undefined') {
                throw('Chart(container_id, param) requires param.');
            }
            this.reload_params(params);
            var required = ['loader', ];
            for (var i = 0; i < required.length; i++) {
                if (!this[required[i]]) {
                    throw('Chart(container_id, param) requires param.' + required[i] + '.');
                }
            }
            
            // init vars
            this.width0 = this.width - this.margin.left - this.margin.right;
            this.height0 = this.height - this.margin.top - this.margin.bottom;
            this.xrange = {
                xdomain: [],
            };

            // initial plot
            this.new_plot();
        }
        catch (e) {
            console.log(e);
        }
    };
    
    Chart.prototype.set_loader = function(loader) {
        this.loader = loader;
    }

    Chart.prototype.load_data = function() {
        var me = this;
        var xr = this.xrange;

        var promise = this.loader(args).then(function(res){
            // nop
        });
        return promise;
    }
    
    Chart.prototype.update_xrange = function() {
        var me = this;
        var xr = me.xrange;
        if (me.xformat) xr.format = me.xformat;
        if (me.xlabelformat) xr.labelformat = me.xlabelformat;
        if (me.xspan) xr.ago = me.xspan;

        xr.xdomain[0] = My.seconds_ago(xr.from + xr.ago);
        xr.xdomain[1] = My.seconds_ago(xr.from);
        var middate = My.seconds_ago(xr.from + xr.ago / 2.0);
        xr.label = My.date_to_string(middate, xr.labelformat);
    }
    
    Chart.prototype.reload = function(params){
        this.reload_params(params);
        return this.update_plot(this);
    }

    Chart.prototype.xshift = function(direction){
        var xr = this.xrange;
        xr.from += direction * xr.ago * 0.2;
        if (xr.from < 0.0) xr.from = 0.0;

        return this.update_plot(this);
    }

    Chart.prototype.reload_params = function(params){
        if (typeof params !== 'undefined') {
            for (var key in params) {
                this[key] = params[key];
            }
        }
    }

    Chart.prototype.new_plot = function(){
        var me = this;
        this.update_xrange();
        this.load_data().then(function() {
            me.d3_init();
            me.d3_plot(false);
        });
    };

    /*
     * load_data() if cache missed, then scale_xy().
     *
     *
     */
    Chart.prototype.update_plot = function(me){
        // update_plot is invoked by setInterval() so here in this
        // context 'this' does not points to Chart instance but window obj.
        // Use 'me' to refer to the instance.
        var xr = me.xrange;
        var xd = xr.xdomain;
        var xdc = xr.xdomain_cache;
        var needreload = false;

        me.update_xrange();

        if (xr.cachedspan != xr.ago) { // nedd to reload since the time resolution changed.
            needreload = true;
        }

        /*
        if (xd[0].getTime() < xdc[0].getTime() || xdc[1].getTime() < xd[1].getTime()) {
            needreload = true;
        }
         */
        var d0 = xd[0].getTime();
        var d1 = xd[1].getTime();
        var c0 = xdc[0].getTime();
        var c1 = xdc[1].getTime();
        var dw = (d1 - d0) * 1.2;
        if (d0 < c0 + dw || c1 - dw < d1) {
            needreload = true;
        }
        
        if (xr.cachedat.getTime() < My.seconds_ago(xr.ago * 0.05).getTime()) {
            needreload = true;
        }
        if (needreload) {
            xr.cachedat = null;
            me.load_data().then(function() {
                me.d3_plot(false);
            });
        }
        else {
            me.d3_plot(true);
        }
    };

    Chart.prototype.d3_init = function() {
        if (!this.sensorname) return;
        var me = this;
        var divid = me.container.str;
        d3.select(divid).select("svg").remove();

        var svg = d3.select(divid).append("svg")
                .attr("width", me.width)
                .attr("height",me.height)
                .append("g")
                .attr("class", "base")
                .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

        me.scale_xy();

        me.xaxis = d3.axisBottom()
            .scale(me.xscale)
            .ticks(5);

        me.yaxis = d3.axisLeft()
            .scale(me.yscale)
            .ticks(3)

        me.line = [];
        var list = [];
        for (var p = 0; p < me.nplots; p++) {
            list[p] = p;
        }
        list.forEach(function(p) { // note!: for(var col...) does not work.
            me.line[p] = d3.line()
                .x(function(d){ return me.xscale(me.parse_date(d.date)); })
                .y(function(d, i){
                    var val = d.val;
                    return me.yscale(val);
                })
                .defined(function(d, i){
                    var val = d.val;
                    if (isNaN(val)) return false;
                    if (val < 0) return false;
                    return val;
                })
        });

        // title
        svg.append("g")
            .append("text")
            .attr("x", me.width0 / 2)
            .attr("y",  0)
            .style("text-anchor", "middle")
            .style("font-size", me.fontsize0)
            .style("text-decoration", "underline")
            .text(me.title);

        // data plot
        var plotid = me.id.monitorid + '-' + me.id.sensorid;
        svg.append("clipPath")
            .attr("id", "clip" + plotid)
            .append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", me.width0)
            .attr("height", me.height0);

        for (var i = me.nplots - 1; 0 <= i; i--) {
            var id = 'line' + i;
            svg.append("g")
                .attr("clip-path", "url(#clip" + plotid + ")")
                .append("path")
                .attr("class", "line")
                .attr("id", id);
        }

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0,  " + me.height0 + ")")
            .call(me.xaxis)
            .selectAll("text")
            .style("font-size", me.fontsize1)
            .style("text-anchor", "center");

        svg.append("g")
            .attr("class", "y axis")
            .call(me.yaxis)
            .selectAll("text")
            .style("font-size", me.fontsize1);

        svg.append("g")
            .append("text")
            .style("font-size", me.fontsize0)
            .attr("class", "ylabel")
            .attr("x", -10)
            .attr("y",  10)
            .attr("text-anchor", "left")
            .text(me.unit);

        svg.append("g")
            .append("text")
            .style("font-size", me.fontsize0)
            .attr("class", "xlabel")
            .attr("x", me.width0 / 2.0)
            .attr("y", me.height0 - 10)
            .text(me.xrange.label)
            .style("text-anchor", "middle");

        // legend
        for (var i = me.nplots - 1; 0 <= i; i--) {
            if (!me.legend[i]) continue;
            var id = 'legend' + i;
            svg.append("g")
                .append("text")
                .attr("x", 100)
                .attr("y",  me.margin.top + 14 * i - 10)
                .attr("id", id)
                .style("text-anchor", "left")
                .style("font-size", me.fontsize0)
                .text(me.legend[i]);
        }
    };

    Chart.prototype.d3_plot = function(do_transition) {
        var me = this;
        var divid = me.container.str;
        var svg = d3.select(divid).selectAll(".base");

        me.scale_xy();

        me.xaxis.scale(me.xscale)
            .tickFormat(d3.timeFormat(me.xrange.format));

        me.yaxis.scale(me.yscale);

        svg.selectAll("g.x")
            .transition().duration(400)
            .call(me.xaxis)
            .selectAll("text")
            .attr("transform", "rotate(-20)")

        svg.selectAll("g.y")
            .transition().duration(400)
            .call(me.yaxis);
        
        for (var i = me.nplots - 1; 0 <= i; i--) {
            var id = 'line' + i;
            var d = svg.selectAll("path#" + id).datum(me.data);
            if (do_transition) {
                d.transition().duration(400).attr("d", me.line[i]);
            }
            else {
                d.attr("d", me.line[i]);
            }
        }

        svg.selectAll(".xlabel").text(me.xrange.label);

        // blinking marker
        svg.select(".marker").remove();
        for (var i = me.data.length - 1; 0 <= i; i--) {
            if (0 <= me.data[i].val) break;
        }
        if (0 < i) {
            svg.append("g")
                .append("circle")
                .attr("class", "marker")
                .attr("cx", me.xscale(me.parse_date(me.data[i].date)))
                .attr("cy", me.yscale(me.data[i].val))
                .attr("r", 4)
                .style("fill", "#00f")
        }
        return 0;
    }
    
    Chart.prototype.scale_xy = function(){
        var xr = this.xrange;
        var me = this;
        var xdom = [
            My.seconds_ago(xr.from + xr.ago),
            My.seconds_ago(xr.from),
        ];

        /*
        var me = d3.timeParse('%Y/%m/%d %H:%M:%S');
        var date0 = My.seconds_ago(xr.from + xr.ago);
        var date1 = My.seconds_ago(xr.from);
        var tz = 9;
        date0.setHours(date0.getHours() - tz);
        date1.setHours(date1.getHours() - tz);
        var xdom = [
            me(My.date_to_string(date0)),
            me(My.date_to_string(date1)),
        ];
         */
        // me.xscale = d3.scaleUtc()
        me.xscale = d3.scaleTime()
            .domain(xdom).range([0, me.width0]);

        var visibledata = me.data.filter(function(d) {
            var t = me.parse_date(d.date);
            if (t < xdom[0] || xdom[1] < t) {
                return false;
            }
            else {
                return d.val;
            }
        });
        
        var ydom = d3.extent(visibledata, function(d){
            return d.val;
        });

        var margin = (ydom[1] - ydom[0]) * 0.2;
        ydom[0] -= margin;
        ydom[1] += margin;
        me.yscale = d3.scaleLinear()
            .domain(ydom)
            .range([me.height0, me.margin.top]);
    };

    // export the constructor
    window.Chart = Chart;
}(jQuery)
