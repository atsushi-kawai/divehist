#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
import os
import re
import argparse

Basedir = './'

ValidDirectives = [
    '@include',
    '@extends',
    '@yield',
    '@section',
    '@stop',
]

def parse_argument():
    ap = argparse.ArgumentParser(description='generate HTML text from given template files.')
    ap.add_argument('infile', nargs='?')
    args = ap.parse_args()
    return args                            

# read lines from a file or stdin.
# returns an array containing the lines as array elements.
#
def read_lines(fname):
    if fname:
        f = open(Basedir + fname)
    else:
        f = sys.stdin
    lines = f.readlines()
    return lines

def include_files(src):
    dst = []
    for line in src:
        m = re.match(r"\s*(@[^(]+)\(", line)
        if m and not m.group(1) in ValidDirectives:
            print("unrecognized directive '{0}'.".format(m.group(1)), file=sys.stderr)
            exit(1)

        m = re.match(r"\s*@include\(\s*'(.*)'\s*\)", line)
        if m:
            print('include {0}'.format(m.group(1)), file=sys.stderr)
            child = tpl2html(read_lines(m.group(1)))
            dst += child
        else:
            dst.append(line)
    return dst

def load_layout(fname):
    lines = read_lines(fname)
    lines = include_files(lines)
    layout = { 'lines': [], 'sections': [] }
    for line in lines:
        m = re.match(r"\s*@yield\(\s*'(.*)'\s*\)", line)
        if m:
            layout['sections'].append(m.group(1))
        layout['lines'].append(line)
    return layout

def tpl2html(lines0):
    lines1 = []
    lines2 = []
    layout = None
    layoutfile = ''
    sections = {}
    secname = ''
    insection = False

    lines0 = include_files(lines0)

    # parse lines, load a layout if found.
    #
    for line in lines0:
        m = re.match(r"\s*@extends\(\s*'(.*)'\s*\)", line)
        if m: # load layout
            if layout:
                print("multiple '@extends' directives found.", file=sys.stderr)
                exit(1)
            layoutfile = m.group(1)
            layout = load_layout(layoutfile)
            continue
        m = re.match(r"\s*@section\(\s*'(.*)'\s*\)", line)
        if m: # start picking up a section
            secname = m.group(1)
            if not secname in layout['sections']:
                print("no section '{0}' in layout '{1}'.".format(secname, layoutfile), file=sys.stderr)
                exit(1)
            sections[secname] = []
            insection = True
            continue
        m = re.match(r"\s*@stop", line)
        if m: # end section
            insection = False
            continue
        if insection: # pick up a line for a section
            sections[secname].append(line)

    if layout: # fill sections
        for line in layout['lines']:
            m = re.match(r"\s*@yield\(\s*'(.*)'\s*\)", line)
            if m:
                sec = m.group(1)
                if sections.has_key(sec):
                    lines1 += sections[sec]
            else:
                lines1.append(line)
    else:
        lines1 = lines0

    return lines1

def main(args=None):
    import subprocess

    if args is None:
        args = parse_argument()

    if args.infile:
        global Basedir
        base = os.path.dirname(args.infile)
        if not base: base = '.'
        Basedir = base + '/'

    lines = read_lines(os.path.basename(args.infile) if args.infile else None)
    html = tpl2html(lines)

    """
    try:
        with open(os.devnull, 'w') as devnull:
            output = subprocess.Popen(['tidy', '-i'], stdin=subprocess.PIPE, stderr=devnull).stdin
    except:
        output = sys.stdout
    """

    output = sys.stdout
    for h in html:
        output.write(h)

if __name__ == "__main__":
    import sys
    sys.exit(main())
