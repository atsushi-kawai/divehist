HTMLNAMES := index.html about.html about-jp.html
DST := ./www
DSTDEV := ./www-dev
HTPLS := $(wildcard htpl/*.htpl)
HTMLS := $(patsubst %,stage/%,$(HTMLNAMES))

TPL2HTML := ./bin/tpl2html.py

.PHONY: all deploy

all:	$(HTMLS)
	echo htmls $(HTMLS)
	make dev

stage/%.html:	htpl/%.htpl htpl/*.htpl
	$(TPL2HTML) ./$< > tmpfile && mv tmpfile $@

dev:
	cp -rp stage/* $(DSTDEV)


deploy:
	mv $(DST) ~/bak/depthanaly-www`date +'%Y%m%d-%H%M'`
	cp -rp $(DSTDEV) $(DST)
