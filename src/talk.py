#!/usr/bin/python
# -*- encoding: utf-8 -*-
from __future__ import print_function
import sys
import stat
import os
import re
import argparse
import datetime
import time

def parse_argument():
    ap = argparse.ArgumentParser(description='read/write from/to a device file. write line is followed by CR LF.')
    ap.add_argument('-d', '--device', default = '/dev/roorka', help='USB serial device file to communicate with the device. e.g./dev/ttyACM0, /dev/cu.')
    ap.add_argument('roofile', nargs='?', help='.roo file to be written.')
    args = ap.parse_args()
    return args

def main(args=None):
    if args is None:
        args = parse_argument()

    # create if device file does not exist yet.
    fdev = open(args.device, 'w')
    fdev.close

    istty = True
    mode = os.fstat(0).st_mode
    if stat.S_ISFIFO(mode) or stat.S_ISREG(mode): istty = False

    fdev = open(args.device, 'r+')
    if args.roofile:
        fdev.write('#ROOT' + chr(13) + chr(10))
        fdev.write('#ROOT' + chr(13) + chr(10))
        fdev.write('#WRITE' + chr(13) + chr(10))
        with open(args.roofile, 'r') as fin:
            for line in fin:
                line = line.strip()
                fdev.write(line + chr(13) + chr(10))
                rline = fdev.readline()
                sys.stdout.write('< ' + rline)
    else:
        while True:
            if istty: print('> ', end='')
            line = sys.stdin.readline().rstrip()
            if not line: break
            fdev.write(line + chr(13) + chr(10))
            rline = fdev.readline()
            sys.stdout.write('< ' + rline)

if __name__ == "__main__":
    import sys
    sys.exit(main())
